<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

include_once dirname(__FILE__) . '/includes/bootstrap.php';
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
  <?php include_once dirname(__FILE__) . '/includes/head.php'; ?>
	<body <?php if ($itemId): ?> id="item<?php echo $itemId; ?>" <?php endif; ?> class="bg-white font-source-sans font-normal text-black leading-normal <?=$bodyclass?>">
		<jdoc:include type="message" />
		<jdoc:include type="component" />
	</body>
</html>
