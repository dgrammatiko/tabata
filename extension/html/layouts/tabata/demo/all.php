<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Layout\LayoutHelper;
?>
<div class="tabata-demo">
	<h1 class="text-5xl">Tabata demo</h1>
	<?=LayoutHelper::render('tabata.demo.buttons')?>
</div>
