<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$displayData = isset($displayData) ? $displayData : [];

/**
 * Layout variables
 *
 * @var   string  $class   Classes to apply. Separated by spaces
 * @var   string  $width   Width to apply to the SVG
 * @var   string  $height  Height to apply to the SVG
 */
extract($displayData);

$class = isset($class) ? $class : 'fill-current h-9 w-9';
$width = isset($width) ? $width : '54';
$height = isset($height) ? $height : '54';
?>
<svg class="<?=$class?>" width="<?=$width?>" height="<?=$height?>" version="1.0" xmlns="https://www.w3.org/2000/svg"
 viewBox="0 0 230.000000 148.000000"
 preserveAspectRatio="xMidYMid meet">
	<metadata>Bow tie</metadata>
	<g transform="translate(0.000000,148.000000) scale(0.100000,-0.100000)" stroke="none">
		<path d="M286 1440 c-114 -36 -215 -129 -251 -230 -8 -23 -15 -70 -15 -104 0
		-80 31 -146 99 -209 47 -44 177 -117 209 -117 7 0 10 -4 7 -10 -3 -5 -22 -10
		-40 -10 -45 0 -127 -31 -170 -65 -143 -108 -131 -374 24 -538 76 -80 143 -110
		263 -115 161 -7 295 34 434 133 63 45 164 150 164 169 0 6 -19 22 -42 36 -51
		31 -120 123 -148 198 -27 72 -36 180 -21 254 25 117 109 241 195 285 l36 19
		-48 59 c-94 117 -236 209 -378 245 -101 26 -237 26 -318 0z"/>
		<path d="M1685 1431 c-139 -39 -277 -127 -365 -235 -22 -26 -40 -52 -40 -56 0
		-4 14 -13 31 -21 48 -19 123 -101 157 -169 79 -158 65 -339 -38 -483 -34 -47
		-106 -107 -143 -120 -32 -10 113 -158 213 -218 107 -64 197 -91 321 -96 129
		-7 190 9 271 67 63 45 127 132 155 212 28 81 25 219 -6 278 -46 89 -125 145
		-219 156 -78 9 -80 14 -14 39 135 52 228 139 258 243 42 144 -49 306 -216 386
		-59 28 -75 31 -180 33 -89 2 -131 -2 -185 -16z"/>
		<path d="M1044 1036 c-102 -47 -164 -158 -164 -291 0 -183 115 -315 275 -315
		160 0 275 132 275 315 0 183 -115 315 -275 315 -42 0 -75 -7 -111 -24z"/>
	</g>
</svg>